'use strict';

const fs = require('fs');
const stylelint = require('stylelint');

const config = require('../');
const inValidStyles = fs.readFileSync('./test/invalid.css', 'utf-8');

describe('flags warnings with invalid css', () => {
  let result;

  beforeEach(() => {
    result = stylelint.lint({
      code: inValidStyles,
      config,
    });
  });

  // it should consists of error
  it('did error', () => {
    return result.then((data) => expect(data.errored).toBeTruthy());
  });

  // checking violations count
  const totalViolations = 2;

  it('flags two warning', () => {
    return result.then((data) => expect(data.results[0].warnings).toHaveLength(totalViolations));
  });

  // validating `number-leading-zero`
  it('warning: number-leading-zero', () => {
    return result.then((data) =>
      expect(data.results[0].warnings[0].text).toBe(
        'Expected a leading zero (number-leading-zero)'
      ));
  });

  it('correct rule flagged', () => {
    return result.then((data) =>
      expect(data.results[0].warnings[0].rule).toBe('number-leading-zero')
    );
  });

  it('correct severity flagged', () => {
    return result.then((data) => expect(data.results[0].warnings[0].severity).toBe('error'));
  });

  it('correct line number', () => {
    return result.then((data) => expect(data.results[0].warnings[0].line).toBe(2));
  });

  it('correct column number', () => {
    return result.then((data) => expect(data.results[0].warnings[0].column).toBe(8));
  });

  it('warning: rule-empty-line-before', () => {
    return result.then((data) =>
      expect(data.results[0].warnings[1].text)
        .toBe('Expected empty line before rule (rule-empty-line-before)'))
  })
});
