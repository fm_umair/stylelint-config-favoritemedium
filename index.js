'use strict';

// https://stylelint.io/user-guide/rules
module.exports = {
  extends: ['stylelint-config-recommended', './rules/best-practice'],
  plugins: ['stylelint-scss']
};
