'use strict';

/**
 * All rules are grouped in alphabetical order and separated by
 * blank line.
 *
 * To navigate to webpage about use following convention:
 *
 * https://stylelint.io/user-guide/<rule-name>
 *
 * for example:
 * https://stylelint.io/user-guide/at-rule-name-case
 */

module.exports = {
  rules: {
    'at-rule-name-case': 'lower',
    'at-rule-name-space-after': 'always-single-line',
    'at-rule-semicolon-newline-after': 'always',
    'at-rule-empty-line-before': [
      'always',
      {
        'except': ['blockless-after-same-name-blockless', 'first-nested'],
        'ignore': ['after-comment'],
      },
    ],

    'at-rule-no-unknown': null,
    'scss/at-rule-no-unknown': true,

    /**
     * Block declaration rules
     */

    'block-closing-brace-empty-line-before': 'never',
    'block-closing-brace-newline-after': 'always',
    'block-closing-brace-newline-before': 'always-multi-line',
    'block-closing-brace-space-before': 'always-single-line',
    'block-opening-brace-newline-after': 'always-multi-line',
    'block-opening-brace-space-after': 'always-single-line',
    'block-opening-brace-space-before': 'always',

    /**
     * Colors naming and declaration rules
     */
    'color-hex-case': 'lower',
    'color-hex-length': 'short',

    /**
     * Commenting rules
     */
    'comment-whitespace-inside': 'always',
    'comment-empty-line-before': [
      'always',
      {
        'except': ['first-nested'],
        'ignore': ['stylelint-commands'],
      },
    ],

    /**
     * Custom properties spacing (--color-primary: red;)
     */
    'custom-property-empty-line-before': 'never',

    'declaration-bang-space-after': 'never',
    'declaration-bang-space-before': 'always',
    'declaration-block-semicolon-newline-after': 'always-multi-line',
    'declaration-block-semicolon-space-after': 'always-single-line',
    'declaration-block-semicolon-space-before': 'never',
    'declaration-block-single-line-max-declarations': 1,
    'declaration-block-trailing-semicolon': 'always',
    'declaration-colon-space-after': 'always-single-line',
    'declaration-colon-space-before': 'never',
    'declaration-empty-line-before': [
      'always',
      {
        'except': ['after-declaration', 'first-nested'],
        'ignore': ['after-comment', 'inside-single-line-block'],
      },
    ],

    'function-comma-newline-after': 'always-multi-line',
    'function-comma-space-after': 'always-single-line',
    'function-comma-space-before': 'never',
    'function-max-empty-lines': 0,
    'function-name-case': 'lower',
    'function-parentheses-newline-inside': 'always-multi-line',
    'function-parentheses-space-inside': 'never-single-line',
    'function-whitespace-after': 'always',
    'length-zero-no-unit': true,
    'max-empty-lines': 1,

    // https://stylelint.io/user-guide/rules/max-nesting-depth
    // root-level at-rules will not be included in the nesting depth calculation
    'max-nesting-depth': 2,
    'indentation': 2,

    'media-feature-colon-space-after': 'always',
    'media-feature-colon-space-before': 'never',
    'media-feature-name-case': 'lower',
    'media-feature-parentheses-space-inside': 'never',
    'media-feature-range-operator-space-after': 'always',
    'media-feature-range-operator-space-before': 'always',
    'media-query-list-comma-newline-after': 'always-multi-line',
    'media-query-list-comma-space-after': 'always-single-line',
    'media-query-list-comma-space-before': 'never',

    'no-eol-whitespace': true,
    'no-missing-end-of-source-newline': true,

    'number-leading-zero': 'always',
    'number-no-trailing-zeros': true,

    'property-case': 'lower',

    'rule-empty-line-before': [
      'always-multi-line',
      {
        'except': ['first-nested'],
        'ignore': ['after-comment'],
      },
    ],

    'selector-attribute-brackets-space-inside': 'never',
    'selector-attribute-operator-space-after': 'never',
    'selector-attribute-operator-space-before': 'never',
    'selector-combinator-space-after': 'always',
    'selector-combinator-space-before': 'always',
    'selector-descendant-combinator-no-non-space': true,
    'selector-list-comma-newline-after': 'always',
    'selector-list-comma-space-before': 'never',
    'selector-max-empty-lines': 0,
    'selector-pseudo-class-case': 'lower',
    'selector-pseudo-class-parentheses-space-inside': 'never',
    'selector-pseudo-element-case': 'lower',
    'selector-pseudo-element-colon-notation': 'double',
    'selector-type-case': 'lower',

    'unit-case': 'lower',

    'value-list-comma-newline-after': 'always-multi-line',
    'value-list-comma-space-after': 'always-single-line',
    'value-list-comma-space-before': 'never',
    'value-list-max-empty-lines': 0,
  },
};
